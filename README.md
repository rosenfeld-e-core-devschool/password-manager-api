# Setup Instructions

Install Ruby and run:

    bundle
    bin/rails server -p 3000

The default front-end api is expected to be hosted on http://localhost:4000. Feel free to change it under config/application.rb.

