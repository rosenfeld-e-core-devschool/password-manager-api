
class Api::V1::SessionsController < ApplicationController
  def current
    unless current_user
      render json: { unauthorized: true }, status: :unauthorized
      return
    end
    render json: { userId: current_user[:user_id], email: current_user[:email], logoutUrl: current_user[:logout_url],
                   csrfToken: form_authenticity_token }
  end

  def logout
    (render json: {invalid_token: true}; return) unless valid_authenticity_token? session, params[:csrf_token].to_s
    url = session[:logout_url]
    reset_session
    redirect_to url || '/'
  end

  def post_test
    render json: { test: 'ok' }
  end
end
