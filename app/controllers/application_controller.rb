class ApplicationController < ActionController::Base
  include AuthSupport
  before_action :authenticate_user!
end
