require "uri"

module AuthSupport
  attr_reader :current_user

  private

  def authenticate_user!
    return if @current_user
    reset_session if Time.now.to_i > (session[:expire_at] || 0)
    set_user_from_session
    return if current_user
    fetch_ory_session if cookies.to_h.keys.any?{|k| k.start_with? "ory_session_"}
  end

  def set_user_from_session
    if (user_id = session[:user_id]) && (email = session[:email]) && (logout_url = session[:logout_url])
      @current_user = {user_id: user_id, email: email, logout_url: logout_url}
      return
    end
  end

  def fetch_ory_session
    puts "fetch_ory_session"
    api_instance = OryClient::FrontendApi.new
    begin
      # Check Who the Current HTTP Session Belongs To
      cookie = request.headers["cookie"]
      ory_session = api_instance.to_session(cookie: cookie)
      identity = ory_session.identity
      uri = URI(Rails.application.config.app_frontend_url)
      base_app_url = "#{uri.scheme}://#{uri.host}:#{uri.port}"
      logout_url = [base_app_url, api_instance.create_browser_logout_flow(cookie: cookie).logout_url].join
      #p ory_session.identity
      session[:expire_at] = 1.hour.from_now.to_i
      user_id = session[:user_id] = identity.id
      email = session[:email] = identity.traits[:email]
      session[:logout_url] = logout_url
      set_user_from_session
    rescue OryClient::ApiError => e
      puts "Error when calling FrontendApi->to_session: #{e}"
      reset_session
    end
  end
end
