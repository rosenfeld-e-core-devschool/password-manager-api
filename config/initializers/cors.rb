require "uri"

Rails.application.config.middleware.insert_before 0, Rack::Cors do
  uri = URI(Rails.application.config.app_frontend_url)
  allow do
    origins "#{uri.host}:#{uri.port}"
    resource '*', headers: :any, methods: [:get, :post, :patch, :put, :delete], credentials: true
  end
end
