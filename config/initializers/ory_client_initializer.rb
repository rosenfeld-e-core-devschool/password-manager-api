require "uri"
require "ory-client"

OryClient.configure do |cfg|
  uri = URI(Rails.application.config.app_frontend_url)
  cfg.server_index = nil
  cfg.scheme = uri.scheme
  cfg.host = "#{uri.host}:#{uri.port}"
  cfg.base_path = uri.path
end

