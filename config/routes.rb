Rails.application.routes.draw do
  match 'console' => 'console#index', via: [:get, :post] if Rails.env.development?
  namespace :api do
    namespace :v1 do
      get 'sessions/current'
      get 'sessions/logout'
      post 'sessions/post_test'
    end
  end
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
