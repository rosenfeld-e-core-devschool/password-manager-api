require "test_helper"

class Api::V1::SessionsControllerTest < ActionDispatch::IntegrationTest
  test "should get current" do
    get api_v1_sessions_current_url
    assert_response :unauthorized
  end
end
